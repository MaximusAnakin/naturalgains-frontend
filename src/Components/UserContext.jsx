import React, { useState, createContext, useEffect} from "react";
import axios from "axios";

export const UserContext = createContext();

export const UserProvider = ({ children }) => {
    const [userId, setUserId] = useState(null); 
	const [name, setName] = useState();
    const [height, setHeight] = useState();
    const [weight, setWeight] = useState();
	const [gender, setGender] = useState();
	const [currentFat, setCurrentFat] = useState();
	const [token, setToken] = useState('');
	const [measures, setMeasures] = useState();

	useEffect(() => {
		const loggedIn = localStorage.getItem("NaturalGainsUserId");
		const token = localStorage.getItem("NaturalGainsToken");
		const firebaseId = localStorage.getItem("NaturalGainsFbId");
		if (loggedIn && !userId) {
			setUserId(loggedIn);
			setToken(token);
			axios({
				method: "get",
				url: `${process.env.REACT_APP_API_URL}/accounts/firebaseId/` + firebaseId,   
				headers: {Authorization: `Bearer ${token}`}
			})
				.then((res) => {
					setHeight(res.data[0].height)
					setGender(res.data[0].gender)
					axios({
						method: "get",
						url: `${process.env.REACT_APP_API_URL}/measurements/last/userId/` + res.data[0]._id,   
						headers: {Authorization: `Bearer ${token}`}
					})
					.then((res) => {
            			setWeight(res.data.weight)
            			setCurrentFat(res.data.fatPercentage)
					})
					.catch((error) => {
						console.log(error);
					});
				})
				.catch((error) => {
					console.log(error);
				});
		}
	}, [userId]);

	return (
		<UserContext.Provider
			value={{
				userId,
                setUserId,
				name,
				setName,
                height,
                setHeight,
                weight,
                setWeight,
                currentFat,
                setCurrentFat,
				gender,
				setGender,
				token,
				setToken,
				measures,
				setMeasures
			}}
		>
			{children}
		</UserContext.Provider>
	);
};
