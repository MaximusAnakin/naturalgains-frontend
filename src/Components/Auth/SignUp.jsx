import { createUserWithEmailAndPassword, signInWithPopup } from "firebase/auth";
import {auth, provider} from "../../firebaseConfig";
import { Visibility, VisibilityOff } from '@mui/icons-material';
import { Alert, Button, IconButton, InputAdornment, TextField, Typography } from '@mui/material'
import { Stack } from '@mui/system';
import React, { useState, useContext } from 'react'
import { useNavigate } from 'react-router';
import { UserContext } from "../UserContext";
import { Link } from "react-router-dom";
import axios from "axios";
import CircularProgress from '@mui/material/CircularProgress';

const SignUp = () => {
    const navigate = useNavigate();
    const {setUserId, setToken} = useContext(UserContext)
    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [errorText, setErrorText] = useState("");
    const [showPassword, setShowPassword] = useState(false);
    const [showProgress, setShowProgress] = useState(false);

    const handleNameChange = e => {
        setName(e.target.value);
    }
    const handleEmailChange = e => {
        setEmail(e.target.value);
    }
    const handlePasswordChange = e => {
        setPassword(e.target.value)
    }
	const handleClickShowPassword = (e) => {
		e.preventDefault();
		setShowPassword(!showPassword);
	}
/*     const signupDenied = (error) => {
		if (error.response.status === 409)
			setErrorText("Username already in use.");
		else setErrorText("Something went wrong. Please try again later.");
	} */
	const containsSpecialChars = (str) => {
		const specialChars = "`!#$%^&*()+=[]{};':\"\\|,<>/?~";
		const result = specialChars.split("").some((specialChar) => {
			if (str.includes(specialChar)) return true;

			return false;
		});
		return result;
	};

    
    const accountToOwnDatabase = (userCredential) => {
        // Gets data from own database, saves token (in context) and sets signedIn
        axios({
            method: "post",
            url: `${process.env.REACT_APP_API_URL}/accounts`,
            headers: {
                Authorization: `Bearer ${userCredential.user.accessToken}`
            },
            data: {
                firebaseId: userCredential.user.uid,
                // sends form data if exists, otherwise (google auth) pulls from firebase response 
                name: name ? name : userCredential.user.displayName,
                email: email ? email : userCredential.user.email
            }
        })
            .then((res) => {
                setToken(userCredential.user.accessToken)
                setUserId(res.data._id)
                localStorage.setItem("NaturalGainsToken", userCredential.user.accessToken);
                localStorage.setItem("NaturalGainsUserId", res.data._id);
                localStorage.setItem("NaturalGainsFbId", userCredential.user.uid);
                navigate("/");
            })
            .catch((error) => {
                setErrorText("Problem with signup");
                console.log(error);
                setShowProgress(false)
            });
    }

    const handleSendForm = (event) => {
        event.preventDefault();
        // First checks input
        const regexEmail = new RegExp("[a-z0-9]+@[a-z]+.[a-z]{2,3}");
		const regexPasswordlower = new RegExp("(?=.*[a-z])");
		const regexPasswordupper = new RegExp("(?=.*[A-Z])");

		if (containsSpecialChars(name)) {
			setErrorText("Name can not contain special characters.");
			return;
		}
		if (name.length < 2) {
			setErrorText("Name should be at least 2 symbols long.");
			return;
		}
        if (password.length < 8) {
			setErrorText("Password length must be at least 8 symbols.");
			return;
		}
		if (
			!regexPasswordlower.test(password) ||
			!regexPasswordupper.test(password)
		) {
			setErrorText(
				"Password must contain a small and a big letter."
			);
			return;
		}
		if (!regexEmail.test(email)) {
			setErrorText("Please check that email is spelled right.");
			return;
		}
        //Creates account in firebase, then if successful creates account in own backend
        createUserWithEmailAndPassword(auth, email, password)
            .then((userCredential) => {
                accountToOwnDatabase(userCredential);
                setShowProgress(true)
            })
            .catch((error) => {
                setErrorText("Problem signing up");
                console.log(error)
                setShowProgress(false)
            });
                
    };

    const signInWithGoogle = () => {
        //Creates account in firebase, then if successful creates account in own backend
        signInWithPopup(auth, provider)
        .then((userCredential) => {
            accountToOwnDatabase(userCredential)
            setShowProgress(true)
        })
        .catch((error) => {
            setErrorText("Problem signing up with Google");
            console.log(error)
            setShowProgress(false)
        });
    }

   
  return (
    <Stack sx={{alignItems: "center"}}>
        {showProgress 
        ? <Stack spacing={2} sx={{position: "absolute", top: "50%", alignItems: "center"}}>
            <CircularProgress  color="inherit" size="2rem" />
            <Typography variant="h5">Patience is a virtue...</Typography>
            </Stack>
        : <Stack spacing={1} className="card">
            <Typography variant="h5">Create account</Typography>
            <Typography variant="body1">Account lets you see your last saved data.</Typography>
            <TextField label="Username" variant="outlined" type="text" required
                value={name}
                onChange={handleNameChange}
                />
            <TextField label="Email" variant="outlined" type="email" required
                value={email}
                onChange={handleEmailChange}
                />
            <TextField label="Password" variant="outlined" required
                value={password}
                onChange={handlePasswordChange}
                type={showPassword ? "text" : "password"}
                InputProps={{
                    endAdornment: (
                        <InputAdornment position="end">
                            <IconButton
                                aria-label="toggle password visibility"
                                tabIndex={-1}
                                onClick={handleClickShowPassword}
                                edge="end"
                            >
                                {showPassword ? <VisibilityOff /> : <Visibility />}
                            </IconButton>
                        </InputAdornment>
                    )
                }}
                />
            <Typography variant="subtitle1">
                By creating an account you accept our 
                <Button size="small" component={Link} to="/privacypolicy">
                    privacy policy
                </Button> and <Button size="small" component={Link} to="/termsofuse">Terms Of Use</Button>. 
            </Typography>    
            {errorText !== "" && (
                <Alert variant="filled" severity="error">
                    {errorText}
                </Alert>
                )}
            <Button variant="contained" onClick={handleSendForm}>Create</Button>
            <Button variant="contained" color="error" onClick={signInWithGoogle}>Google sign in</Button>
            <Button size="small" component={Link} to="/signin">Log in instead</Button>                        
        </Stack>
        }
    </Stack>
  )
}

export default SignUp