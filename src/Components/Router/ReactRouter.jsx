import React from "react";
import { Routes, Route } from "react-router-dom";
import Home from "../Home/Home";
import SignUp from "../Auth/SignUp";
import SignIn from "../Auth/SignIn";
import PrivacyPolicy from "../Pages/PrivacyPolicy";
import History from "../History/History";
import TermsOfUse from "../Pages/TermsOfUse";

const ReactRouter = () => (
	<Routes>
		<Route path="/" element={<Home />} />
		<Route path="/signup" element={<SignUp />} />
		<Route path="/signin" element={<SignIn />} />
		<Route path="/privacypolicy" element={<PrivacyPolicy />} />
		<Route path="/termsofuse" element={<TermsOfUse />} />
		<Route path="/history" element={<History />} />
		<Route path="*" element={<>Page not found</>} />
	</Routes>
);



export default ReactRouter;

