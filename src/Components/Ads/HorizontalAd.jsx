import React, { useEffect } from 'react';

const HorizontalAd = () => {
  useEffect(() => {
    const script = document.createElement('script');
    script.src = "https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-3530217827895239";
    script.async = true;
    document.body.appendChild(script);
  }, []);

  return (
    <div className="ad">
      <ins className="adsbygoogle"
           style={{ display: 'block' }}
           data-ad-client="ca-pub-3530217827895239"
           data-ad-slot="4512962010"
           data-ad-format="auto"
           data-full-width-responsive="true">
        </ins>
    </div>
  );
};

export default HorizontalAd;
