import { Typography } from '@mui/material';
import { Stack } from '@mui/system';
import axios from 'axios';
import React, { useContext, useEffect } from 'react'
import { ResponsiveContainer, LineChart, Line, CartesianGrid, XAxis, YAxis, Tooltip, Legend } from 'recharts';
import { UserContext } from '../UserContext';

const History = () => {
  const {measures, setMeasures, userId, token} = useContext(UserContext)

  useEffect(() => {
        // Gets measurements data
      if (token) {
        axios.get(`${process.env.REACT_APP_API_URL}/measurements/userId/`+ userId, {
          headers: { Authorization: `Bearer ${token}` }
        })
        .then((res) => {
          setMeasures(res.data)
        })
        /* setShowProgress(false) */
        .catch ((err) => { 
            /* setErrorText("Problem getting measurements"); */
            console.log(err);
            /* setShowProgress(false) */
            })
          }
    }, [token, setMeasures, userId]) 

      // When measures is pulled from db, this iterates every entry and maps properties to datakeys and labels
      const data = measures ? measures.map(day => {
        const dataToShow = {};
        const date = new Date(day.date);
        dataToShow.label = date.toLocaleDateString();

        dataToShow.Weight = day.weight;
        dataToShow.Fat = day.fatPercentage;
        return dataToShow;
      }) : [];
      

  return (
     <Stack >
      <Stack className="card">
        <Typography textAlign="center" variant="h6">Your progress</Typography>
        <Stack className="section-content">
          <ResponsiveContainer  height={300}>
            <LineChart data={data} margin={{ top: 10, right: 10, bottom: 0, left: -20 }}>
              <Tooltip />
              <XAxis dataKey="label" />
              <YAxis />
              <CartesianGrid stroke="#ccc" strokeDasharray="0" />
              <Legend/>
              <Line type="monotone" dataKey="Weight" stroke="#17A8F5" />
              <Line type="monotone" dataKey="Fat" stroke="#FB8833"/>
            </LineChart>
          </ResponsiveContainer>
        </Stack>
      </Stack>
    </Stack>
  )
}

export default History