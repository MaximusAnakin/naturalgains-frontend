import Header from "./Components/Navigation/Header";
import { BrowserRouter } from "react-router-dom";
import ReactRouter from "./Components/Router/ReactRouter";
import { UserProvider } from "./Components/UserContext";
import Footer from "./Components/Navigation/Footer";

function App() { 
  return (
    <UserProvider>
        <BrowserRouter className="app">
          <Header />
          <ReactRouter />
          <Footer />
        </BrowserRouter>
    </UserProvider>
  );
}

export default App;
