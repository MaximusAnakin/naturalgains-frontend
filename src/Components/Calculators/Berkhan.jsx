import React, {useContext, useState} from 'react';
import TextField from '@mui/material/TextField';
import { InputAdornment, Stack, Typography } from '@mui/material';
import { UserContext } from '../UserContext';


export default function Berkhan() {
    const {height, weight, currentFat, gender} = useContext(UserContext);
    const [fatGoal, setFatGoal] = useState(gender === "male" ? 12 : 20);

    const handleFatGoalChange = e => {
        if (e.target.value < 0) {
			    e.target.value = 0;
        }
        setFatGoal(e.target.value)
    }

    let fatFreeMass = weight * ((100-currentFat)/100);
    let fatFreeMassPotential = ((height - 100) / 1.05) * (gender === "male" ? 1 : 0.81);

    let weightPotential = (fatFreeMassPotential * (fatGoal / 100 + 1)).toFixed(1);
    let muscleToGain = (fatFreeMassPotential -  fatFreeMass).toFixed(1);

    let fatToLose = (weight - (fatFreeMass / ((100-fatGoal)/100))).toFixed(1);

  return (
    <Stack  spacing={2} className="card" sx={{maxWidth: "250px", m: 1}}>
      <Typography textAlign="center" variant="h6">Basic formula</Typography>
      <Stack direction="row">
        <Typography>Body fat goal: </Typography>
        <TextField sx={{mx: 1, width:'7ch'}} variant="standard" type="number"
          value={fatGoal}
          onChange={handleFatGoalChange}
          InputProps={{endAdornment: <InputAdornment position="end">%</InputAdornment>}}/>
      </Stack>
       {height > 99 && ( <Typography>Weight potential: <b>{weightPotential} kg</b></Typography>)}
       {height > 99 && weight > 30 && ( <Typography>Muscle to gain: <b>{muscleToGain} kg</b></Typography>)}
       {currentFat > 0 && height > 99 && weight > 30 && ( <Typography>Fat to lose with<br/>current muscle: <b>{fatToLose} kg</b></Typography>)}
    </Stack>
  );
}
