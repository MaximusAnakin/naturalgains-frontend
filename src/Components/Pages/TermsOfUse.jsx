import { Stack, Typography } from '@mui/material'
import React from 'react'

const TermsOfUse = () => {
  return (
    <Stack alignItems="center" >
        <Stack  component="ol" spacing={1} paddingLeft="50px" className="card" textAlign="left"> 
        <Typography textAlign="center" variant='h4'>Terms of Use</Typography>
        <Typography >Welcome to NaturalGains, an app designed to help you achieve your fitness goals through natural methods! By using our app, you agree to the following terms and conditions:</Typography>
        <Typography component="li" variant="h6">Use of the App</Typography>
        <Typography>
            NaturalGains grants you a limited, non-exclusive, non-transferable, and revocable license to use the app for your personal, non-commercial use only. You may not use the app for any illegal or unauthorized purpose, and you agree to comply with all applicable laws and regulations.
        </Typography>
        <Typography component="li" variant="h6">User Accounts</Typography>
        <Typography>
            To use certain features of the app, you may need to create a user account. You agree to provide accurate and complete information when creating your account, and to keep your login credentials confidential. You are responsible for all activity that occurs under your account.
        </Typography>
        <Typography component="li" variant="h6">Content</Typography>
        <Typography>
            The app may allow you to upload or submit content, such as photos, videos, or comments. You retain ownership of any content you submit, but you grant NaturalGains a non-exclusive, royalty-free, transferable, sub-licensable, worldwide license to use, copy, modify, distribute, and display your content for the purposes of operating and improving the app.
        </Typography>
        <Typography component="li" variant="h6">Prohibited Conduct</Typography>
        <Typography>
            You agree not to engage in any of the following activities while using the app:
        </Typography>
        <Typography>
            Posting, transmitting, or sharing any content that is unlawful, threatening, abusive, harassing, defamatory, vulgar, obscene, or otherwise objectionable;
        </Typography>
        <Typography>
            Using the app to violate the rights of others, including their privacy rights, intellectual property rights, or right to a fair trial;
        </Typography>
            Impersonating any person or entity, or falsely representing your affiliation with any person or entity;
        <Typography>
            Attempting to interfere with the proper functioning of the app, including by hacking, phishing, or using any other method to gain unauthorized access to the app or its data;
        </Typography>
        <Typography>
            Collecting or storing any personally identifiable information of other users without their consent.
        </Typography>
        <Typography component="li" variant="h6">Disclaimer of Warranties</Typography>
        <Typography>
            The app is provided on an "as is" and "as available" basis, without any warranties of any kind, express or implied. NaturalGains does not guarantee that the app will be free from errors or interruptions, or that it will meet your specific requirements. You agree to use the app at your own risk.
        </Typography>
        <Typography component="li" variant="h6">Limitation of Liability</Typography>
        <Typography>
            To the fullest extent permitted by law, NaturalGains and its affiliates, officers, directors, employees, and agents will not be liable for any damages, including but not limited to direct, indirect, incidental, consequential, or punitive damages arising out of or relating to your use of the app, even if NaturalGains has been advised of the possibility of such damages.
        </Typography>
        <Typography component="li" variant="h6">Indemnification</Typography>
        <Typography>
            You agree to indemnify and hold harmless NaturalGains and its affiliates, officers, directors, employees, and agents from and against any claims, liabilities, damages, losses, and expenses, including reasonable attorneys' fees, arising out of or in any way connected with your use of the app or your violation of these terms.
        </Typography>
        <Typography component="li" variant="h6">Changes to the Terms</Typography>
        <Typography>
            NaturalGains reserves the right to modify these terms at any time, without prior notice. Your continued use of the app after such modifications constitutes your acceptance of the revised terms.
        </Typography>
        <Typography component="li" variant="h6">Governing Law</Typography>
        <Typography>
            These terms and your use of the app will be governed by and construed in accordance with the laws of the jurisdiction in which NaturalGains is located, without giving effect to any principles of conflicts of law.
        </Typography>
        <Typography component="li" variant="h6">Entire Agreement</Typography>
        <Typography>
            These terms constitute the entire agreement between you and NaturalGains regarding your use of the app. If any provision of these terms is found to be invalid or unenforceable, the remaining provisions will remain in full force and effect.
        </Typography>
        </Stack>
    </Stack>
  )
}

export default TermsOfUse