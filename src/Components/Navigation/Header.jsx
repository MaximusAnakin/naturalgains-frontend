import React, {useContext, useState} from 'react';
import {Box, Toolbar, IconButton, Typography, AppBar, Menu, Container, Button, MenuItem} from '@mui/material';
import MenuIcon from '@mui/icons-material/Menu';
import FitnessCenterIcon from '@mui/icons-material/FitnessCenter';
import { Link } from 'react-router-dom';
import { useNavigate } from 'react-router';
import { UserContext } from '../UserContext';
import { signOut } from 'firebase/auth';
import { auth } from '../../firebaseConfig';

function Header() {
  const navigate = useNavigate();
  const [anchorElNav, setAnchorElNav] = useState(null);
  const {setToken, userId, setUserId, setGender, setHeight, setWeight, setCurrentFat} = useContext(UserContext);

  //Navigation buttons
  const pages = userId? [
    /* {name: user.name, path: '/'},  */
    {name: 'Calculator', path: '/'},
    {name: 'History', path: '/history'}
  ] : [
    {name: 'Login', path: '/signin'}, 
  ];

  const handleOpenNavMenu = (event) => {
    setAnchorElNav(event.currentTarget);
  };

  const handleCloseNavMenu = () => {
    setAnchorElNav(null);
  };

  const handleSignout = () => {
    signOut(auth)
    .then(() => {
      localStorage.removeItem("NaturalGainsToken");
		  localStorage.removeItem("NaturalGainsUserId");
		  localStorage.removeItem("NaturalGainsFbId");
      handleCloseNavMenu();
      setToken("");
      setUserId();
      setGender();
		  setHeight();
		  setWeight();
		  setCurrentFat();
      navigate("/");
    })
    .catch(error => {
      console.log(error)
    });
  }

  return (
    <AppBar position="relative" sx={{ backgroundColor: "#25242c", marginBottom: "20px", maxHeight: "65px"}}>
      <Container maxWidth="xl">
        <Toolbar disableGutters >

          {/* Desktop header*/}
          <Box sx={{ flexGrow: 0,  display: { xs: 'none', md: 'flex' } }}>
            <FitnessCenterIcon sx={{mr: 1 }} />
            <Typography
              variant="h6"
              noWrap
              component={Link}
              to="/"
              sx={{
                mr: 2,
                display: { xs: 'none', md: 'flex' },
                fontFamily: 'monospace',
                fontWeight: 700,
                letterSpacing: '.1rem',
                color: 'inherit',
                textDecoration: 'none',
              }}
            >
              NaturalGains
            </Typography>
          </Box>
            <Box sx={{ flexGrow: 1, justifyContent: "end", display: { xs: 'none', md: 'flex' } }}>
              {pages.map((page) => (
                <Button
                  key={page.name}
                  component={Link}
                  to={page.path}
                  onClick={handleCloseNavMenu}
                  sx={{ my: 2, color: 'white', display: 'block' }}
                >
                  {page.name}
                </Button>
              ))}
              {userId 
              ? <Button onClick={handleSignout}>
                  Logout
                </Button>
              : <Button
                  variant="contained" 
                  component={Link}
                  to={"/signup"}
                  sx={{ my: 2}}>
                    Sign up
                </Button>
              }
            </Box>

          {/* Mobile header*/}
          <FitnessCenterIcon sx={{ display: { xs: 'flex', md: 'none' }, mr: 1 }} />
          <Typography
            variant="h5"
            noWrap
            component={Link}
            to="/"
            sx={{
              mr: 2,
              display: { xs: 'flex', md: 'none' },
              flexGrow: 1,
              fontFamily: 'monospace',
              fontWeight: 700,
              letterSpacing: '.1rem',
              color: 'inherit',
              textDecoration: 'none',
            }}
          >
            NaturalGains
          </Typography>
          <Box sx={{ flexGrow: 0, display: { xs: 'flex', md: 'none' } }}>
            <IconButton
              size="large"
              aria-label="account of current user"
              aria-controls="menu-appbar"
              aria-haspopup="true"
              onClick={handleOpenNavMenu}
              color="inherit"
            >
              <MenuIcon />
            </IconButton>
            <Menu
              id="menu-appbar"
              anchorEl={anchorElNav}
              anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'left',
              }}
              keepMounted
              transformOrigin={{
                vertical: 'top',
                horizontal: 'left',
              }}
              open={Boolean(anchorElNav)}
              onClose={handleCloseNavMenu}
              sx={{
                display: { xs: 'block', md: 'none' },
              }}
            >
              {pages.map((page) => (
                <MenuItem 
                  key={page.name}
                  component={Link}
                  to={page.path}
                  onClick={handleCloseNavMenu}>
                  <Typography textAlign="center">{page.name}</Typography>
                </MenuItem>
              ))}
              {userId
              ? <Button sx={{ml: 1}} onClick={handleSignout}>
                  Logout
                </Button>
              : <Button
                  variant="contained" 
                  component={Link}
                  to={"/signup"}
                  sx={{ mx: 0}}>
                    Sign up
                </Button>}
            </Menu>
          </Box>
        </Toolbar>
      </Container>
    </AppBar>
  );
}
export default Header;
