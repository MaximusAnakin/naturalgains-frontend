import { Alert, Button, FormControlLabel, InputAdornment, Radio, RadioGroup, TextField, Typography } from '@mui/material'
import { Stack } from '@mui/system';
import axios from 'axios';
import React, { useContext, useState } from 'react'
import { UserContext } from '../UserContext';
import CircularProgress from '@mui/material/CircularProgress';
import { Link } from 'react-router-dom';

const InitialUserInfo = () => {
    const {userId, name, height, setHeight, weight, setWeight, currentFat, setCurrentFat, gender, setGender} = useContext(UserContext);
    const [showProgress, setShowProgress] = useState(false);
    const [successBar, setSuccessBar] = useState("");
    const [errorBar, setErrorBar] = useState("");

    const handleHeightChange = e => {
        if (e.target.value < 0) {
		    e.target.value = 0;
        }
        setHeight(e.target.value);
    }
    const handleWeightChange = e => {
        if (e.target.value < 0) {
		    e.target.value = 0;
        }
        setWeight(e.target.value);
    }
    const handleFatChange = e => {
        if (e.target.value < 0) {
			e.target.value = 0;
        }
        setCurrentFat(e.target.value)
    }
    const handleGenderChange = e => {
        setGender(e.target.value)
        e.target.value === "male" ? setCurrentFat(15) : setCurrentFat(25);
    }

    const handleInfoUpdate = (e) => {
        e.preventDefault();
        setSuccessBar("");
        setShowProgress(true)
        basicInfoUpdate();
        measurementsUpdate();
    }

    const basicInfoUpdate = () => {
        axios({
            method: "patch",
            url: `${process.env.REACT_APP_API_URL}/accounts/` + userId,
            headers: {
                Authorization: `Bearer ${localStorage.getItem("NaturalGainsToken")}`
            },
            data: {
                name: name,
                height: height,
                gender: gender,
            }
        })
            .then((res) => {
                setSuccessBar("Saved!");
            })
            .catch((error) => {
                setShowProgress(false)
                setErrorBar("Problem saving your info. Please try again later.")
            });
    }  

    const measurementsUpdate = () => {
        axios({
            method: "post",
            url: `${process.env.REACT_APP_API_URL}/measurements/`,
            headers: {
                Authorization: `Bearer ${localStorage.getItem("NaturalGainsToken")}`
            },
            data: {
                userId: userId,
                fatPercentage: currentFat,
                weight: weight,
            }
        })
            .then((res) => {
                setShowProgress(false)
                setSuccessBar("Saved!");
            })
            .catch((error) => {
                setErrorBar("Problem saving your info. Please try again later.")
                setShowProgress(false)
            });
    }
    

  return (
    <form onSubmit={handleInfoUpdate}  >
        <Stack spacing={2} className="card" sx={{maxWidth: "250px", m: 1}}>
                <Typography textAlign="center" variant="h6">Enter basic info</Typography>
                <RadioGroup
                row
                value={gender ?? ""}
                onChange={handleGenderChange}
            >
                    <FormControlLabel value="male" control={<Radio />} label="Male" />
                    <FormControlLabel value="female" control={<Radio />} label="Female"  />
            </RadioGroup>
                <TextField label="Your height" variant="outlined" type="number" 
                value={height ?? ""}
                onChange={handleHeightChange}
                InputProps={{
                    endAdornment: <InputAdornment position="end">cm</InputAdornment>,
                }}/>
            <TextField label="Current weight" variant="outlined" type="number"
                value={weight ?? ""}
                onChange={handleWeightChange}
                InputProps={{
                    endAdornment: <InputAdornment position="end">kg</InputAdornment>,
                }}/>
            <TextField label="Current fat %" variant="outlined" type="number"
                value={currentFat ?? ""}
                onChange={handleFatChange}
                InputProps={{
                    endAdornment: <InputAdornment position="end">%</InputAdornment>,
                }}/>
            {successBar && userId && 
                <Alert severity="success">
                    {successBar}
                </Alert>}
            {errorBar && userId && 
                <Alert severity="error">
                    {errorBar}
                </Alert>}
            {userId &&
                <>
                    <Button variant="contained" type="submit">
                        {showProgress 
                        ? <CircularProgress color="inherit" size="25px" />
                        : "Save" }
                    </Button>
                    <Button component={Link} to="/history">Your progress</Button>
                </>
                }                
        </Stack>
    </form>
  )
}

export default InitialUserInfo