import { signInWithEmailAndPassword, signInWithPopup  } from "firebase/auth";
import { auth, provider } from "../../firebaseConfig";
import { Visibility, VisibilityOff } from '@mui/icons-material';
import { Alert, Button, IconButton, InputAdornment, TextField, Typography } from '@mui/material'
import { Stack } from '@mui/system';
import React, { useState, useContext } from 'react'
import { useNavigate } from 'react-router';
import { UserContext } from "../UserContext";
import { Link } from "react-router-dom";
import axios from "axios";
import CircularProgress from '@mui/material/CircularProgress';

const SignIn = () => {
    const navigate = useNavigate();
    const {setGender, setUserId, setHeight, setWeight, setCurrentFat} = useContext(UserContext)
    const [emailField, setEmailField] = useState(""); 
    const [password, setPassword] = useState("");
    const [errorText, setErrorText] = useState("");
    const [showPassword, setShowPassword] = useState(false);
    const [showProgress, setShowProgress] = useState(false);

    const handleEmailChange = e => {
        setEmailField(e.target.value);
    }
    const handlePasswordChange = e => {
        setPassword(e.target.value)
    }
	const handleClickShowPassword = (e) => {
		e.preventDefault();
		setShowPassword(!showPassword);
	}
    /* const signupDenied = (error) => {
		if (error.response.status === 409)
			setErrorText("Username already in use.");
		else setErrorText("Something went wrong. Please try again later.");
	} */
	/* const containsSpecialChars = (str) => {
		const specialChars = "`!#$%^&*()+=[]{};':\"\\|,<>/?~";
		const result = specialChars.split("").some((specialChar) => {
			if (str.includes(specialChar)) return true;

			return false;
		});
		return result;
	}; */


    const getMeasurements = async (userId, token) => {
        // Gets latest measurements data
        try {
            const res = await fetch(`${process.env.REACT_APP_API_URL}/measurements/last/userId/`+ userId, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            })
            const data = await res.json()
            setWeight(data.weight)
            setCurrentFat(data.fatPercentage)
            setShowProgress(false)
        }
        catch (err) { 
            setErrorText("Problem getting measurements");
            console.log(err);
            setShowProgress(false)
            }
        }
        
    const getAccountData = (userCredential) => {
        // Gets data from own database, saves token (in context) and sets signedIn
        axios({
            method: "get",
            url: `${process.env.REACT_APP_API_URL}/accounts/firebaseId/` + userCredential.user.uid,   
            headers: {
                Authorization: `Bearer ${userCredential.user.accessToken}`
            }
        })
            .then((res) => {
                setUserId(res.data[0]._id)
                setHeight(res.data[0].height)
                setGender(res.data[0].gender)
                localStorage.setItem("NaturalGainsToken", userCredential.user.accessToken);
                localStorage.setItem("NaturalGainsUserId", res.data[0]._id);
                localStorage.setItem("NaturalGainsFbId", userCredential.user.uid);
                getMeasurements(res.data[0]._id, userCredential.user.accessToken)
                navigate("/");
            })
            .catch((error) => {
                setErrorText("Problem getting account data");
                setShowProgress(false)
                console.log(error);
            });
    };   

    const handleSendForm = (event) => {
        event.preventDefault();
        //Signs in to firebase
        signInWithEmailAndPassword (auth, emailField, password)
            .then((userCredential) => {
                getAccountData(userCredential)
                setShowProgress(true)
            })
            .catch((error) => {
                setErrorText("Problem signing in");
                console.log(error)
                setShowProgress(false)
            });
    };

    const signInWithGoogle = () => {
        //Signs in to firebase
        signInWithPopup(auth, provider)
            .then((userCredential) => {
                getAccountData(userCredential)
                setShowProgress(true)
            })
            .catch((error) => {
                setErrorText("Problem signing in with Google");
                console.log(error)
                setShowProgress(false)
            });
    };
   
  return (
    <Stack sx={{alignItems: "center"}}>
        {showProgress 
        ? <Stack spacing={2} sx={{position: "absolute", top: "50%", alignItems: "center"}}>
            <CircularProgress  color="inherit" size="2rem" />
            <Typography variant="h5">Patience is a virtue...</Typography>
            </Stack>
        : <Stack spacing={1} className="card">
            <Typography variant="h5">Sign In</Typography>
            <TextField label="Email" variant="outlined" type="email" required
                value={emailField}
                onChange={handleEmailChange}
                />
            <TextField label="Password" variant="outlined" required
                value={password}
                onChange={handlePasswordChange}
                type={showPassword ? "text" : "password"}
                InputProps={{
                    endAdornment: (
                        <InputAdornment position="end">
                            <IconButton
                                aria-label="toggle password visibility"
                                tabIndex={-1}
                                onClick={handleClickShowPassword}
                                edge="end"
                            >
                                {showPassword ? <VisibilityOff /> : <Visibility />}
                            </IconButton>
                        </InputAdornment>
                    )
                }}
                />
            {errorText !== "" && (
                <Alert variant="filled" severity="error">
                    {errorText}
                </Alert>
                )}
            <Button variant="contained" onClick={handleSendForm}>
                Sign In
            </Button>
            <Button variant="contained" color="error" onClick={signInWithGoogle}>
                Google sign in
            </Button>
            <Button size="small" component={Link} to="/signup">
                Sign up instead
            </Button>            
        </Stack>
        }
    </Stack>
  )
}

export default SignIn