import { Stack, Typography } from '@mui/material'
import React from 'react'

const PrivacyPolicy = () => {
  return (
    <Stack alignItems="center">
        <Stack  spacing={1} className="card" textAlign="left"> 
          <Typography textAlign="center" variant='h4'>Privacy Policy</Typography>

          <Typography>At NaturalGains, we take your privacy very seriously. This privacy policy explains how we collect, use, and protect your personal information when you use our app.</Typography>
          
          <Typography variant="h6">Information We Collect:</Typography>

          <Typography>When you use our app, we may collect personal information that you provide to us such as your name, email address, and any other information you choose to provide.</Typography>
          
          <Typography>We may also automatically collect certain information about your use of our app, such as your IP address, device type, and browser type.</Typography>
          
          <Typography variant="h6">How We Use Your Information:</Typography>
          
          <Typography>We may use your personal information to provide you with the services you have requested, to improve our app, and to communicate with you about our services.</Typography>
          
          <Typography>We may also use your information for research purposes, to analyze user behavior, and to improve our marketing efforts.</Typography>
          
          <Typography variant="h6">How We Protect Your Information:</Typography>
          
          <Typography>We take reasonable measures to protect your personal information from unauthorized access, disclosure, or destruction. We use industry-standard security measures such as encryption, firewalls, and secure servers to protect your information.</Typography>
          
          <Typography>We do not sell or rent your personal information to third parties for their marketing purposes.</Typography>
          
          <Typography variant="h6">Third-Party Services:</Typography>
          
          <Typography>We may use third-party services to help us operate our app, such as hosting services or analytics providers. These third-party services may have access to your personal information only to perform specific tasks on our behalf and are required not to disclose or use your information for any other purpose.</Typography>
          
          <Typography>We use Google AdSense to serve ads on our web app. Google AdSense uses cookies to collect information about your use of our web app and other websites, such as your IP address, browser type, and the pages you visit. This information may be transmitted to and stored by Google on servers in the United States. Google may use this information to show you ads that are more relevant to your interests, to limit the number of times you see a particular ad, and to measure the effectiveness of ad campaigns.</Typography>
          
          <Typography>You can opt-out of Google's use of cookies for personalized advertising by visiting Google's Ads Settings at <a href="https://www.google.com/settings/ads">https://www.google.com/settings/ads</a>.</Typography>
          
          <Typography>We use third-party ad networks to serve ads on our web app. These ad networks may use cookies to collect information about your use of our web app and other websites, such as your IP address, browser type, and the pages you visit. This information may be used to show you ads that are more relevant to your interests, to limit the number of times you see a particular ad, and to measure the effectiveness of ad campaigns.</Typography>
          
          <Typography>You can opt-out of some third-party ad networks' use of cookies for personalized advertising by visiting the Network Advertising Initiative opt-out page at <a href="http://optout.networkadvertising.org/">http://optout.networkadvertising.org/</a> or the Digital Advertising Alliance's opt-out page at <a href="http://optout.aboutads.info/">http://optout.aboutads.info/</a>.</Typography>
          
          <Typography variant="h6">Links to Other Websites:</Typography>
          
          <Typography>Our app may contain links to other websites. We are not responsible for the privacy practices or content of these websites. We encourage you to read the privacy policies of these websites before providing any personal information.</Typography>
          
          <Typography variant="h6">Changes to This Privacy Policy:</Typography>
          
          <Typography>We may update this privacy policy from time to time. We will notify you of any changes by posting the new privacy policy on our app. You are advised to review this privacy policy periodically for any changes.</Typography>
        </Stack>
    </Stack>
    
  )
}

export default PrivacyPolicy