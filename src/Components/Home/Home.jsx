import { Typography } from '@mui/material'
import { Stack } from '@mui/system'
import React, { useContext } from 'react'
import HorizontalAd from '../Ads/HorizontalAd'
import Berkhan from '../Calculators/Berkhan'
import { UserContext } from '../UserContext'
import InitialUserInfo from '../UserInfo/UserInfo'

const Home = () => {
  const {userId} = useContext(UserContext);
  
  return (
    <Stack sx={{alignItems: "center"}}>
      <HorizontalAd />
      {!userId && 
        <Stack className="card" maxWidth="500px">
            <Typography variant="h4">Achieve your Natural Potential</Typography>
            <Typography>
              Know thyself. Find out your genetic muscle growth potential.
              Visualizing your goal is the stronges motivator. 
              <br/>Try it out! No signup required.
            </Typography>
        </Stack>
      }
      <Stack sx={{ alignItems: { xs: 'center', sm: 'start' }, flexDirection: { xs: 'column', sm: 'row' }}}>
          <InitialUserInfo />
          <Berkhan />
      </Stack>
    </Stack>
    
  )
}

export default Home