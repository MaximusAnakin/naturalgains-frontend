# NaturalGains

NaturalGains web app allows you to check your natural muscle gain potential based on a few body measurements. Save your measurements and track progress over time on a graph. User account can be created with Google account or email. Project is created with a MERN stack (React, Node, Express, MongoDB) and utilises Firebase for authentication. 

Live version: https://naturalgainsapp.onrender.com/

Backend code: https://gitlab.com/MaximusAnakin/naturalgains-fullstack

Creator: Maxim Anikin
