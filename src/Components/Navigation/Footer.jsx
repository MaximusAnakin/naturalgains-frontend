import { Button} from '@mui/material'
import React from 'react'
import { Link } from 'react-router-dom'

const Footer = () => {
    
  return (
    <div>
        <div className='footer'>
            <Button sx={{color: "#25242c"}} component={Link} to={"/privacypolicy"}>Privacy Policy</Button>
            <Button sx={{color: "#25242c"}} component={Link} to={"/termsofuse"}>Terms Of Use</Button>
        </div>
    </div>
    
  )
}

export default Footer